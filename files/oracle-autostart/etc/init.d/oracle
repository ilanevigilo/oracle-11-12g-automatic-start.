#!/bin/sh
# Copyright (c) 2009, Evigilo
# All rights reserved.
#
# Startup script for Oracle 11/12G based on Red Hat Linux 5/6/7
#
# Author: Yaniv Marom-Nachumi <yaniv@evigilo.net>
#
# Edit the /etc/oratab file. 
# Database entries in the oratab file appear in the following format: 
#    ORACLE_SID:ORACLE_HOME:{Y|N}
# where Y or N specifies whether you want the dbstart and dbshut
# scripts to start up and shut down the database.
#
# chkconfig: - 75 15
# config: /etc/oratab
# config: /etc/sysconfig/oracle
# pidfile: /var/run/oracle.pid
# description: Oracle 11/12G auto start init.d script.
#
### BEGIN INIT INFO
# Provides: oracle
# Required-Start: $local_fs $remote_fs $network
# Required-Stop: $local_fs $remote_fs $network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: start and stop oracle
### END INIT INF

# Source function library.
. /etc/rc.d/init.d/functions

if [ -f /etc/sysconfig/oracle ]; then
    . /etc/sysconfig/oracle
else
    exit 0
fi

RETVAL=0
ORACLE_PIDFILE=${PIDFILE-/var/run/oracle.pid}

# get environment according to oracle owner home (e.g. ORACLE_HOME)
test -f /home/${ORA_OWNER}/.bash_profile && . /home/${ORA_OWNER}/.bash_profile
if [ -z "$ORACLE_HOME" -o ! -d "$ORACLE_HOME" -o -z "$ORACLE_SID" ]; then
    echo "Cannot find ORACLE_HOME directory, or ORACLE_SID not set."
    echo -n "Environment settings are wrong? Check /home/${ORA_OWNER}/.bash_profile"
    failure $"Checking Oracle environment"
    exit 1
fi

start() {
    if [ -f $ORACLE_PIDFILE ]; then
        read ppid < $ORACLE_PIDFILE
        if [ `ps --pid $ppid 2> /dev/null | grep -c $ppid 2> /dev/null` -eq '1' ]; then
            echo -n "Oracle DB is already running"
            failure
            echo
            return 1 
        else
            rm -f $ORACLE_PIDFILE
        fi
    fi
    
    # the database will not start if no entries in /etc/oratab are "Y"
    grep -s -q ":Y" /etc/oratab >&/dev/null || {
        echo -n "No entries in /etc/oratab are Y"
        failure $"Checking /etc/oratab"
        exit 1
    }

    if [ ! -f $ORACLE_HOME/bin/dbstart -o -z "$ORA_OWNER" ]; then
        echo "No such file: \$ORACLE_HOME/bin/dbstart"
        echo -n "Oracle could not be found (ORACLE_HOME wrong?)" 
        failure $"Checking for $ORACLE_HOME/bin/dbstart"
        exit 1
    fi
  
    echo -n $"Starting Oracle: "
    daemon --user $ORA_OWNER --pidfile=${ORACLE_PIDFILE} $ORACLE_HOME/bin/dbstart
    RET1=$?
    if [ "${START_LISTENER:-no}" = "yes" ] && [ -x $ORACLE_HOME/bin/lsnrctl ]; then
        daemon --user $ORA_OWNER --pidfile=${ORACLE_PIDFILE} $ORACLE_HOME/bin/lsnrctl start
        RET2=$?
    fi
     
    if [ $RET1 -eq 0 ] && [ ${RET2:-0} -eq 0 ]; then
        touch ${ORACLE_PIDFILE} 
        echo_success
		echo
        return 0
    else
		failure
		echo
		return 1
    fi
}

stop() {
    echo -n "Stopping Oracle: "

    # the database will not stop if no entries in /etc/oratab are "Y"
    grep -s -q ":Y" /etc/oratab >&/dev/null || {
        echo -n "No entries in /etc/oratab are Y"
        failure $"Checking /etc/oratab"
        exit 1
    }

    if [ ! -f $ORACLE_HOME/bin/dbshut -o -z "$ORA_OWNER" ]; then
        echo "No such file: \$ORACLE_HOME/bin/dbstart"
        echo -n "Oracle could not be found (ORACLE_HOME wrong?)" 
        failure $"Checking for $ORACLE_HOME/bin/dbshut"
        exit 1
    fi

   
    if [ "${START_LISTENER:-no}" = "yes" ] && [ -x $ORACLE_HOME/bin/lsnrctl ]; then
        su - $ORA_OWNER -c "$ORACLE_HOME/bin/lsnrctl stop" > /dev/null 2>&1
        RET2=$?
    fi

    su - $ORA_OWNER -c "$ORACLE_HOME/bin/dbshut" > /dev/null 2>&1
    RET1=$?
    
    if [ $RET1 -eq 0 ] && [ ${RET2:-0} -eq 0 ]; then
        rm -f ${ORACLE_PIDFILE}
        success
		echo
        return 0
    else
		failure
		echo
		return 1
      fi
}

case "$1" in
  start)
      start
      ;;
  stop)
      stop
      ;;
  restart)
      stop
      start
      ;;
  condrestart)
      [ -f /var/lock/subsys/oracle ] && restart || :
      ;;
  *)
      echo "Usage: $0 {start|stop|restart|condrestart}"
      exit 1
esac
exit $?
