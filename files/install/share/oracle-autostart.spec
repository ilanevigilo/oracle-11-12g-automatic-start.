# Copyright (c) 2009, Evigilo
# All rights reserved.
#
%define _topdir	    ${basedir}/target	 
%define buildroot   %{_topdir}/%{name}-%{version}-root
%define _sourcedir  %{_topdir}
%define __jar_repack 0

Summary:	Oracle 11/12G Autostart init.d scripts.
Name:		oracle-autostart
Version:	${project.version}
Release:	${buildNumber}
License:	LGPL
Group:		Applications/System
Source:		oracle-autostart-${project.version}.tar.gz
Packager:	Yaniv Marom-Nachumi <yaniv@evigilo.net>
Vendor:		Evigilo LTD
URL:		https://bitbucket.org/yanivnachumi/oracle-11-12g-automatic-start
BuildRoot:	%{buildroot}
BuildArch:	noarch


%description
Oracle 11/12G Autostart init.d scripts 

%prep
rm -rf "${RPM_BUILD_ROOT}"

%setup -n oracle-autostart-${project.version}

%build

%install
DESTDIR="${RPM_BUILD_ROOT}" \
	./install.sh
	
%clean
rm -rf $RPM_BUILD_ROOT $RPM_BUILD_DIR/%{name}-%{version}/

%files
%defattr(755,root,root)
/etc/init.d/oracle
%defattr(-,root,root)
/etc/sysconfig/oracle

%changelog
