#!/bin/sh

die() {
	local localmsg="$1"
	echo "FATAL: ${localmsg}" >&2
	exit 1
}

get_full_path() {
	local p="$1"
	( cd "${p}" ; pwd ) 
}

install_recursive() {
	local src="$1"
	local dst="$2"
	local d
		
	install -m 0755 -d "${DESTDIR}${dst}"
	( cd "${SOURCE_DIR}/${src}" && find . -type d ) | while read d; do
		if [ "${d}" != "." ]; then
	        [ -d "${DESTDIR}${dst}/${d}" ] || install -m 0755 -d "${DESTDIR}${dst}/${d}" || die "Cannot create ${d}"
		fi
	done || die "Cannot copy recursive ${src}"
	
	( cd "${SOURCE_DIR}/${src}" && find . -type f ) | while read f; do
		local mask
		[ "${f%%.sh}" == "${f}" ] && mask="0644" || mask="0755"
		install -m "${mask}" "${SOURCE_DIR}/${src}/${f}" "${DESTDIR}${dst}/${f}" || die "Cannot install ${f}"
	done || die "Cannot copy recursive ${src}"
	
	return 0
}

install_files() {
	SOURCE_DIR="$(get_full_path $(dirname "$0"))"
	
	install_recursive oracle-autostart/etc "/etc"
}

main() {
	install_files
}

main
exit 0
